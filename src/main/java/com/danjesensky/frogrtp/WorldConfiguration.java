package com.danjesensky.frogrtp;

import org.bukkit.Location;

import java.util.Random;

public interface WorldConfiguration {
    boolean isWithinRegion(Location loc);
    Location generateWithinRegion(Random r);
}
