package com.danjesensky.frogrtp;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.Random;

public class LocationFinderTask implements Runnable {
    public static Random rng;
    private FrogRTP plugin;
    private ConfigurationManager config;
    private Player p;
    private Location currentLocation;

    static{
        rng = new Random();
    }

    public LocationFinderTask(FrogRTP plugin, ConfigurationManager config, Player p, Location currentLocation){
        this.plugin = plugin;
        this.config = config;
        this.p = p;
        this.currentLocation = currentLocation;
    }

    @Override
    public void run() {
        long startMillis = System.currentTimeMillis();
        long castTime = this.config.getCastTime()*1000;
        String world = this.p.getWorld().getName();
        WorldConfiguration worldConfig = this.config.getWorldConfig(world);

        Location destination;
        int count = 0;
        do{
            destination = worldConfig.generateWithinRegion(rng);
            count++;
        }while(destination == null);
        this.plugin.getLogger().info("RTP took "+count+" attempts.");

        long time = System.currentTimeMillis() - startMillis;
        long cast = Math.max(0, castTime-time);

        Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new RTPTask(
                this.plugin,
                this.p,
                this.currentLocation,
                destination), this.p.hasPermission("frogrtp.ignorecasttime") ? 0 : cast/50);

        this.plugin.getLogger().info("Took "+time+"ms to find a spot to RTP to. Remaining waiting period is: "+cast+"ms.");
    }
}
