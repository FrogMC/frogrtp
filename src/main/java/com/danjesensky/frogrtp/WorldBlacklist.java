package com.danjesensky.frogrtp;

import java.util.List;

public class WorldBlacklist {
    private List<String> worlds;
    private BlacklistMode mode;

    public WorldBlacklist(List<String> worlds, BlacklistMode mode){
        this.worlds = worlds;
        this.mode = mode;
    }

    public boolean isWorldPermissible(String world){
        switch(this.mode){
            case WHITELIST:
                return this.worlds.contains(world);
            case BLACKLIST:
                return !this.worlds.contains(world);
            case NONE:
            default:
                return true;
        }
    }

    public enum BlacklistMode{
        WHITELIST,
        BLACKLIST,
        NONE
    }
}
