package com.danjesensky.frogrtp;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class RTPTask implements Runnable {
    private FrogRTP plugin;
    private Player p;
    private Location destination;
    private Location current;

    public RTPTask(FrogRTP plugin, Player p, Location current, Location destination){
        this.plugin = plugin;
        this.p = p;
        this.current = current;
        this.destination = destination;
    }

    @Override
    public void run(){
        long cooldown = this.plugin.getCooldowns().containsKey(p.getName()) ?
                this.plugin.getCooldowns().get(p.getName()) :
                0;
        if(cooldown > System.currentTimeMillis()){
            this.p.sendMessage("You appear to have replayed your teleportation request. Canceled.");
            return;
        }

        if(p.getWorld().getName().equals(current.getWorld().getName())
        && ((p.getLocation().getBlockX() == current.getBlockX()
        && p.getLocation().getBlockY() == current.getBlockY()
        && p.getLocation().getBlockZ() == current.getBlockZ()) || this.p.hasPermission("frogrtp.ignorecasttime"))){
            this.plugin.getCooldowns().put(p.getName(), System.currentTimeMillis()+(1000*this.plugin.getConfigManager().getCooldown()));
            this.p.teleport(destination);
        }else{
            p.sendMessage("Sorry, you moved. Teleport cancelled.");
            this.plugin.getCooldowns().remove(this.p.getName());
        }
    }
}
