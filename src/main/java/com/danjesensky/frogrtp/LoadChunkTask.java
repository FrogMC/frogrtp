package com.danjesensky.frogrtp;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;

public class LoadChunkTask implements Runnable{
    private final Object monitor;
    private String world;
    private int x, chunkX;
    private int z, chunkZ;
    private Location location;

    public LoadChunkTask(String world, int x, int z, Object monitor){
        this.world = world;
        this.x = x;
        this.chunkX = x/16;
        this.z = z;
        this.chunkZ = z/16;
        this.monitor = monitor;
    }

    @Override
    public void run() {
        synchronized (monitor) {
            try {
                World w = Bukkit.getWorld(this.world);
                Chunk c = w.getChunkAt(chunkX, chunkZ);
                c.load(true);
                int y = w.getHighestBlockYAt(x, z);

                FrogRTP.getInstance().getLogger().info("y: "+y);
                Block block = w.getBlockAt(x,y-1,z);
                if (block.isEmpty() || block.isLiquid() || y < w.getSeaLevel() || !w.getBlockAt(x,y,z).isEmpty() || !w.getBlockAt(x,y+1,z).isEmpty()) {
                    this.location = null;
                }else {
                    this.location = new Location(Bukkit.getWorld(world), x+0.5, y+2, z+0.5);
                    FrogRTP.getInstance().getLogger().info(location.getX()+","+location.getY()+","+location.getZ());
                }
            }finally {
                this.monitor.notify();
            }
        }
    }

    public Location getResult(){
        return location;
    }
}
