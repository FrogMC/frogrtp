package com.danjesensky.frogrtp;

import com.google.common.util.concurrent.Monitor;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.Random;

public class RectangularWorldConfiguration implements WorldConfiguration {
    private FrogRTP plugin;
    private String world;
    private int minX;
    private int minZ;
    private int maxX;
    private int maxZ;
    private final Object monitor;

    public RectangularWorldConfiguration(FrogRTP plugin, String world, int minX, int minZ, int maxX, int maxZ){
        this.plugin = plugin;
        this.world = world;
        this.monitor = new Object();

        if(minX > maxX){
            this.minX = maxX;
            this.maxX = minX;
        }else {
            this.maxX = maxX;
            this.minX = minX;
        }

        if(minZ > maxZ){
            this.minZ = maxZ;
            this.maxZ = minZ;
        }else {
            this.maxZ = maxZ;
            this.minZ = minZ;
        }
    }

    public int getMinX() {
        return minX;
    }

    public int getMinZ() {
        return minZ;
    }

    public int getMaxX() {
        return maxX;
    }

    public int getMaxZ() {
        return maxZ;
    }

    @Override
    public boolean isWithinRegion(Location loc){
        return world.equals(loc.getWorld().getName()) &&
                loc.getX() > this.minX &&
                loc.getX() < this.maxX &&
                loc.getZ() > this.minZ &&
                loc.getZ() < this.maxZ;
    }

    @Override
    public Location generateWithinRegion(Random r){
        int maxX = this.maxX - minX;
        int maxZ = this.maxZ - minZ;

        int x = r.nextInt(maxX)+minX;
        int z = r.nextInt(maxZ)+minZ;

        synchronized (this.monitor){
            try {
                LoadChunkTask loadTask = new LoadChunkTask(world, x, z, this.monitor);
                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, loadTask, 5L);
                this.monitor.wait();
                return loadTask.getResult();
            }catch(InterruptedException ex){
                return null;
            }
        }
    }
}
