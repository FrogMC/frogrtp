package com.danjesensky.frogrtp;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class ConfigurationManager {
    private FrogRTP plugin;
    private YamlConfiguration config;
    private final File dataDir;
    private final File configFile;

    public ConfigurationManager(FrogRTP plugin) throws IOException, InvalidConfigurationException {
        this.plugin = plugin;
        this.dataDir = plugin.getDataFolder();
        this.configFile = new File(dataDir, "config.yml");
        this.loadConfig();
    }

    public YamlConfiguration getConfig(){
        return this.config;
    }

    public void loadConfig() throws IOException, InvalidConfigurationException {
        if (!configFile.exists()) {
            this.saveDefaultConfig();
        }
        this.config = new YamlConfiguration();
        this.config.load(configFile);
    }

    public void saveDefaultConfig() throws IOException {
        if (!dataDir.exists() && !dataDir.mkdir()) {
            throw new IOException("Failed to create the directory for configuration.");
        }
        try (final InputStream fis = ConfigurationManager.class.getResourceAsStream("/config.yml");
             final FileOutputStream fos = new FileOutputStream(configFile)) {

            byte[] buffer = new byte[1024];
            int read;

            while ((read = fis.read(buffer, 0, buffer.length)) != -1) {
                fos.write(buffer, 0, read);
            }
        }
    }

    public void setValue(String key, Object value){
        this.config.set(key, value);
    }

    public void save() throws IOException {
        this.config.save(this.configFile);
    }

    public int getCooldown(){
        return this.config.getInt("cooldown");
    }

    public int getCastTime(){
        return this.config.getInt("castTime");
    }

    public WorldConfiguration getWorldConfig(String world){
        ConfigurationSection sect = this.config.getConfigurationSection("worlds");
        if(!sect.contains(world)){
            return null;
        }
        // maybe other types later
        return new RectangularWorldConfiguration(
                this.plugin,
                world,
                sect.getInt(world+".minX"),
                sect.getInt(world+".minZ"),
                sect.getInt(world+".maxX"),
                sect.getInt(world+".maxZ")
        );
    }

    public WorldBlacklist getBlacklist(){
        return new WorldBlacklist(
                this.config.getStringList("worldBlacklist"),
                WorldBlacklist.BlacklistMode.valueOf(this.config.getString("worldBlacklistMode")));
    }
}
