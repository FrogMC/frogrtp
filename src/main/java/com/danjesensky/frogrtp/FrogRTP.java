package com.danjesensky.frogrtp;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class FrogRTP extends JavaPlugin {
    private Logger log;
    private ConfigurationManager config;
    private volatile Map<String, Long> cooldowns;
    private static FrogRTP instance;

    public FrogRTP(){
        this.log = Logger.getLogger(getClass().getName());
        this.cooldowns = new HashMap<>();
        instance = this;
    }

    public static FrogRTP getInstance(){
        return instance;
    }

    @Override
    public void onEnable() {
        // make sure the random number generator is initialized via static initializer
        // (basically guaranteeing, though mostly likely anyways, that the classloader picked the class up).
        new LocationFinderTask(null, null, null, null);
        try {
            this.config = new ConfigurationManager(this);
        }catch(Exception ex){
            this.log.severe("Failed to get config: "+ex.getMessage());
            this.getPluginLoader().disablePlugin(this);
            return;
        }
        log.info("Enabled FrogRTP version @VERSION@.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(label.equalsIgnoreCase("rtpreload")){
            if(!sender.hasPermission("frogrtp.admin")){
                sender.sendMessage("Sorry, but you don't have permission to do that.");
                return true;
            }
            ConfigurationManager old = this.config;
            try {
                this.config = new ConfigurationManager(this);

                sender.sendMessage("FrogRTP configuration successfully reloaded.");
            }catch(Exception ex){
                sender.sendMessage("Something went wrong with your configuration. Trying to use the old one.");
                this.config = old;
            }
            return true;
        }

        if(!(sender instanceof Player)){
            sender.sendMessage("Sorry, but you have to be a player to use that.");
            return true;
        }

        if(label.equalsIgnoreCase("rtp")){
            if(!sender.hasPermission("frogrtp.use")){
                sender.sendMessage("Sorry, but you don't have permission to use that.");
                return true;
            }

            Player player = (Player)sender;
            if(!this.config.getBlacklist().isWorldPermissible(player.getWorld().getName())){
                player.sendMessage("Sorry, but you can't do that in this world.");
                return true;
            }
            long time = System.currentTimeMillis();

            if(this.cooldowns.containsKey(sender.getName())){
                long cooldown = this.cooldowns.get(sender.getName());
                if(cooldown > time){
                    if(!player.hasPermission("frogrtp.ignorecooldown")){
                        sender.sendMessage("Sorry, you have to wait to use that again: "+(cooldown-time)/1000+"s");
                        return true;
                    }else{
                        this.cooldowns.remove(player.getName());
                    }
                }
            }

            int waitTime = this.config.getCastTime();
            if(waitTime > 0 && !player.hasPermission("frogrtp.ignorecasttime")){
                sender.sendMessage("Looking for a spot to teleport you to. Don't move for about "+waitTime+" seconds, or you'll cancel it.");
            }else{
                sender.sendMessage("Looking for a spot to teleport you to.");
            }
            Bukkit.getScheduler().scheduleAsyncDelayedTask(this, new LocationFinderTask(this, this.config, player, player.getLocation()));
            return true;
        }
        return false;
    }

    public Map<String, Long> getCooldowns(){
        return this.cooldowns;
    }

    public ConfigurationManager getConfigManager(){
        return this.config;
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @Override
    public Logger getLogger(){
        return this.log;
    }
}